package nurgiza.birdy;

import android.content.Context;
import android.database.Cursor;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

public class TweetAdapter extends CursorAdapter {
    private LayoutInflater mInflater;

    public TweetAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
        mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public void bindView(View itemView, Context ctx, Cursor dataCursor) {
        ViewHolder twHolder = (ViewHolder) itemView.getTag();
        if (twHolder != null) {
            twHolder.tvText.setText(dataCursor.getString(dataCursor.getColumnIndex(TweetDatabase.C_TEXT)));
            twHolder.tvUser.setText(dataCursor.getString(dataCursor.getColumnIndex(TweetDatabase.C_USER)));
            twHolder.tvCreated.setText(formatTimeInCreated(
                    dataCursor.getString(dataCursor.getColumnIndex(TweetDatabase.C_CREATED)))
            );
        }
    }

    @Override
    public View newView(Context ctx, Cursor cur, ViewGroup parent) {
        View view = mInflater.inflate(R.layout.row, parent, false);
        ViewHolder holder = new ViewHolder(view);
        view.setTag(holder);
        return view;
    }

    public static class ViewHolder {
        TextView tvCreated, tvUser, tvText;
        ViewHolder(View itemView) {
            tvUser = (TextView) itemView.findViewById(R.id.textUser);
            tvCreated = (TextView) itemView.findViewById(R.id.textCreatedAt);
            tvText = (TextView) itemView.findViewById(R.id.textText);
        }
    }

    public static String formatTimeInCreated(String createdLine)
    {
        String delimiter = "-";
        String newDelimiter = ":";
        int index = createdLine.lastIndexOf(delimiter);
        String formatedFirst =
                new StringBuilder(createdLine).replace(index, index+1, newDelimiter).toString();
        index = formatedFirst.lastIndexOf(delimiter);
        String formated =
                new StringBuilder(formatedFirst).replace(index, index+1, newDelimiter).toString();

        return  formated;
    }
}