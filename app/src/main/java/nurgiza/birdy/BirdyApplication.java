package nurgiza.birdy;


import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.util.Log;

import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterAuthToken;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.models.Tweet;
import com.twitter.sdk.android.core.services.StatusesService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import io.fabric.sdk.android.Fabric;

public class BirdyApplication extends Application {
    private static final String TAG = BirdyApplication.class.getSimpleName();

    private static final String TWITTER_KEY = "MY_KEY";
    private static final String TWITTER_SECRET = "MY_SECRET";

    public static final long INTERVAL_NONE = 0;

    private final String sessionSavedKey = "saved";
    private final String sessionTokenKey = "session_token";
    private final String sessionUserNameKey = "session_user";
    private final String sessionIdKey = "session_id";
    private final String sessionUserIdKey = "user_id";

    private SimpleDateFormat sourceDateFormat;
    private SimpleDateFormat targetDateFormat;

    private TweetDatabase tweetDatabase;

    private SharedPreferences prefs;
    long insertedId = 0;

    @Override
    public void onCreate() {
        super.onCreate();

        initDateFormatters();

        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig));

        tweetDatabase = new TweetDatabase(this);

        prefs = PreferenceManager.getDefaultSharedPreferences(this);
    }

    private void initDateFormatters(){
        sourceDateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy", Locale.ENGLISH);
        targetDateFormat = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
        Calendar cal = Calendar.getInstance();
        TimeZone tz = cal.getTimeZone();
        targetDateFormat.setTimeZone(tz);
    }

    public TweetDatabase getTweetDatabase() {
        return tweetDatabase;
    }

    public void saveSession(TwitterSession session) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(sessionTokenKey, session.getAuthToken().toString());
        editor.putString(sessionUserNameKey, session.getUserName());
        editor.putLong(sessionIdKey, session.getId());
        editor.putLong(sessionUserIdKey, session.getUserId());
        editor.putBoolean(sessionSavedKey, true);
        editor.commit();
    }

    //get token from fabric.io serialized string
    @Nullable
    private TwitterAuthToken tokenFromString(String serialized) {
        String[] parts = serialized.split(",");
        if (parts.length == 2) {
            String partToken = parts[0];
            String partSecret = parts[1];
            String token = partToken.substring(partToken.indexOf("=") + 1);
            String secret = partSecret.substring(partSecret.indexOf("=") + 1);

            return new TwitterAuthToken(token, secret);
        }
        else
            return null;
    }

    public boolean login() {
        if (prefs.contains(sessionSavedKey)) {
            String sessionToken = prefs.getString(sessionTokenKey, "");
            long sessionId = prefs.getLong(sessionIdKey, 0);
            long userId = prefs.getLong(sessionUserIdKey, 0);
            String sessionUser = prefs.getString(sessionUserNameKey, "");

            TwitterAuthToken auth = tokenFromString(sessionToken);

            if (!auth.isExpired()) {
                TwitterSession session = new TwitterSession(auth, userId, sessionUser);
                Twitter.getSessionManager().setSession(sessionId, session);

                return true;
            }
            else
            {
                SharedPreferences.Editor editor = prefs.edit();
                editor.clear();
                editor.commit();
            }
        }
        return false;
    }

    public void logout()
    {
        //stop service
        stopService(new Intent(this, UpdaterService.class));

        //remove alarm task
        Intent intent = new Intent(this, UpdaterService.class);
        PendingIntent pendingIntent = PendingIntent.getService(this, -1, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarmManager.cancel(pendingIntent);

        //login from fabric.io
        Twitter.getSessionManager().clearActiveSession();
        Twitter.logOut();

        //drop all tweets in db
        tweetDatabase.delete();

        //remove preferences
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
        editor.commit();
    }

    public long getStatuses(){
        TwitterApiClient twitterApiClient = TwitterCore.getInstance().getApiClient();
        StatusesService statusesService = twitterApiClient.getStatusesService();

        String lastCreated = tweetDatabase.getLastCreated();

        //get last updates
        statusesService.homeTimeline(getTweetCount(), null, null, null, null, null, null,
                new Callback<List<Tweet>>() {
                    @Override
                    public void success(Result<List<Tweet>> tweets) {
                        Date date;
                        ContentValues values = new ContentValues();

                        for (Tweet tw : tweets.data) {
                            try {
                                date = sourceDateFormat.parse(tw.createdAt);
                            }
                            catch (ParseException e)
                            {
                                Log.d(TAG, "Parse date fail");
                                date = new Date();
                            }
                            values.put(TweetDatabase.C_CREATED, targetDateFormat.format(date));
                            values.put(TweetDatabase.C_TEXT, tw.text);
                            values.put(TweetDatabase.C_USER, tw.user.screenName);
                            values.put(TweetDatabase.C_TWEET_ID, tw.id);

                            //put in database
                            insertedId = getTweetDatabase().insertOrIgnore(values);
                        }
                    }

                    @Override
                    public void failure(TwitterException e) {
                        Log.d(TAG, "Twitter get homeline fail");
                    }
                });

        return tweetDatabase.countRows(lastCreated, insertedId);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        tweetDatabase.close();
        Log.i(TAG, "onTerminate");
    }

    public long getInterval() {
        if (!getUpdatesOn()) {
            return INTERVAL_NONE;
        } else {
            return Long.parseLong(prefs.getString("interval", String.valueOf(INTERVAL_NONE)));
        }
    }

    public String getUsername()
    {
        String username = prefs.getString(sessionUserNameKey, "");

        return username;
    }

    public int getTweetCount()
    {
        String count = prefs.getString(getString(R.string.tweetCountKey), "60000");

        return Integer.parseInt(count);
    }

    public boolean getUpdatesOn()
    {
        boolean isOn = prefs.getBoolean(getString(R.string.statusesUpdatesKey), false);

        return isOn;
    }
}
