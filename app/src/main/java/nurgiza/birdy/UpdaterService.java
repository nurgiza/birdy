package nurgiza.birdy;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.util.Log;

import com.twitter.sdk.android.core.TwitterException;

public class UpdaterService extends IntentService {
    private static final String TAG = UpdaterService.class.getSimpleName();

    public static final String NEW_STATUS_INTENT = "nurgiza.birdy.NEW_STATUS";
    public static final String NEW_STATUS_EXTRA_COUNT = "NEW_STATUS_EXTRA_COUNT";
    public static final String RECEIVE_TIMELINE_NOTIFICATIONS = "nurgiza.birdy.RECEIVE_TIMELINE_NOTIFICATIONS";

    private NotificationManager notificationManager;
    private Notification notification;
    BirdyApplication app;

    public UpdaterService() {
        super(TAG);
        Log.d(TAG, "Constructed");
    }

    @Override
    protected void onHandleIntent(Intent inIntent) {
        Log.d(TAG, "onHandleIntent");

        notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        app = (BirdyApplication) getApplication();

        long newRows = 0;
        try {
            newRows = app.getStatuses();
        } catch (TwitterException e) {
            Log.e(TAG, "Failed to connect to twitter service", e);
        }

        if (newRows > 0) {
            Intent intent;
            intent = new Intent(NEW_STATUS_INTENT);
            intent.putExtra(NEW_STATUS_EXTRA_COUNT, 1);
            sendBroadcast(intent, RECEIVE_TIMELINE_NOTIFICATIONS);
            sendTimelineNotification(newRows);
        }
    }

    private void sendTimelineNotification(long timelineUpdateCount) {
        Log.d(TAG, "sendTimelineNotification");

        PendingIntent pendingIntent = PendingIntent.getActivity(this, -1,
                new Intent(this, TimeLineActivity.class),
                PendingIntent.FLAG_UPDATE_CURRENT);

        CharSequence notificationTitle = getText(R.string.msgNotificationTitle);
        CharSequence notificationSummary = getString(R.string.msgNotificationMessage, timelineUpdateCount);

        notification = new Notification.Builder(this)
                .setContentTitle(notificationTitle)
                .setContentText(notificationSummary)
                .setSmallIcon(R.mipmap.icon)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true).build();
        notification.when = System.currentTimeMillis();
        notification.flags |= Notification.FLAG_AUTO_CANCEL;

        notificationManager.notify(0, this.notification);
    }

}