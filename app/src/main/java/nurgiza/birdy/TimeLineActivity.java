package nurgiza.birdy;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.CursorLoader;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ListView;


public class TimeLineActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {
    private BirdyApplication app;

    private TweetAdapter adapter;
    private ListView lvItems;
    private SwipeRefreshLayout swipeRefreshLayout;
    private Button btnDoTwitt;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timeline);

        app = (BirdyApplication) getApplication();

        initSwipeRefresher();

        getSupportLoaderManager().initLoader(0,null,this);

        initWriteButton();

        startService(new Intent(TimeLineActivity.this, UpdaterService.class));
    }

    private void initWriteButton(){
        btnDoTwitt = (Button) findViewById(R.id.buttonTwitt);
        btnDoTwitt.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(TimeLineActivity.this, StatusActivity.class);
                        startActivity(intent);
                    }
                }
        );
    }

    private void initSwipeRefresher(){
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefresh);
        swipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        startService(new Intent(TimeLineActivity.this, UpdaterService.class));
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }
        );
        swipeRefreshLayout.setColorSchemeColors(Color.RED, Color.GREEN, Color.BLUE, Color.CYAN);

        initList();
    }

    private void initList(){
        lvItems = (ListView) findViewById(R.id.tweetList);
        adapter = new TweetAdapter(this, null, 0);
        lvItems.setAdapter(adapter);
        lvItems.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            //nothing
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (lvItems.getChildAt(0) != null) {
                    swipeRefreshLayout.setEnabled(lvItems.getFirstVisiblePosition() == 0 &&
                            lvItems.getChildAt(0).getTop() == 0);
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(this,DatabaseProvider.CONTENT_URI,
                null,
                null,
                null,
                TweetDatabase.ORDER_BY_CREATION_DESC);
    }

    @Override
    public void onLoadFinished(Loader loader, Cursor cursor) {
        adapter.swapCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader loader) {
        adapter.swapCursor(null);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    void finishActivity()
    {
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.actionSettings:
                Intent intent = new Intent(this, PrefsActivity.class);
                startActivity(intent);
                return true;
            case R.id.actionLogout:
                app.logout();
                finishActivity();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
