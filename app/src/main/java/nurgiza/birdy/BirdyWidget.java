package nurgiza.birdy;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.util.Log;
import android.widget.RemoteViews;

public class BirdyWidget extends AppWidgetProvider {
    private static final String TAG = BirdyWidget.class.getSimpleName();

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        Cursor c = context.getContentResolver().query(DatabaseProvider.CONTENT_URI,
                null, null, null, TweetDatabase.ORDER_BY_CREATION_DESC);
        try {
            RemoteViews views = new RemoteViews(context.getPackageName(),
                    R.layout.widget_last_update);

            views.setOnClickPendingIntent(R.id.appIcon, PendingIntent
                    .getActivity(context, 0, new Intent(context,
                            MainActivity.class), 0));

            if (c.moveToFirst()) {
                CharSequence user = c.getString(c.getColumnIndex(TweetDatabase.C_USER));
                CharSequence createdAt = TweetAdapter.formatTimeInCreated(
                        c.getString(c.getColumnIndex(TweetDatabase.C_CREATED)));
                CharSequence message = c.getString(c.getColumnIndex(TweetDatabase.C_TEXT));

                for (int appWidgetId : appWidgetIds) {
                    Log.d(TAG, "Updating widget " + appWidgetId);

                    views.setTextViewText(R.id.textUser, user);
                    views.setTextViewText(R.id.textCreatedAt, createdAt);
                    views.setTextViewText(R.id.textText, message);

                    appWidgetManager.updateAppWidget(appWidgetId, views);
                }
            } else {
                Log.d(TAG, "No data to update");
            }
        } finally {
            c.close();
        }
        Log.d(TAG, "onUpdated");
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        if (intent.getAction().equals(UpdaterService.NEW_STATUS_INTENT)) {
            Log.d(TAG, "onReceived detected new status update");
            AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
            this.onUpdate(context, appWidgetManager, appWidgetManager
                    .getAppWidgetIds(new ComponentName(context, BirdyWidget.class)));
        }
    }
}