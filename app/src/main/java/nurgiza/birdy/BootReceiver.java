package nurgiza.birdy;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class BootReceiver extends BroadcastReceiver {

    private static final String TAG = BootReceiver.class.getSimpleName();;

    public void onReceive(Context context, Intent callingIntent) {

        long interval = ((BirdyApplication) context.getApplicationContext()).getInterval();
        if (interval == BirdyApplication.INTERVAL_NONE)
            return;

        Intent intent = new Intent(context, UpdaterService.class);
        PendingIntent pendingIntent = PendingIntent.getService(context, -1, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        AlarmManager alarmManager = (AlarmManager) context
                .getSystemService(Context.ALARM_SERVICE);
        alarmManager.setInexactRepeating(AlarmManager.ELAPSED_REALTIME, System
                .currentTimeMillis(), interval, pendingIntent);

        Log.d(TAG, "onReceived");
    }

}