package nurgiza.birdy;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;


public class MainActivity extends AppCompatActivity {
    private TwitterLoginButton btnLogin;
    private Button btnTimeline;
    private BirdyApplication app;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        app = (BirdyApplication) getApplication();
        initLoginButton();
        initTimelineButton();
        if (app.login()){
            btnTimeline.setText(btnTimeline.getText() + " " + app.getUsername());
            startTimelineActivity();
        }

        switchButtons();
    }

    @Override
    protected void onResume(){
        super.onResume();
        switchButtons();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        btnLogin.onActivityResult(requestCode, resultCode, data);
    }

    private void switchButtons(){
        if (app.login()){
            btnTimeline.setVisibility(View.VISIBLE);
        }
        else
        {
            btnTimeline.setVisibility(View.GONE);
        }
    }

    private void initLoginButton() {
        btnLogin = (TwitterLoginButton) findViewById(R.id.twitterLoginButton);
        btnLogin.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                TwitterSession session = result.data;
                app.saveSession(session);
                startTimelineActivity();
            }
            @Override
            public void failure(TwitterException exception) {
                Log.d("TwitterKit", "Login with Twitter failure", exception);
            }
        });
    }

    private void initTimelineButton() {
        btnTimeline = (Button) findViewById(R.id.timelineButton);
        btnTimeline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, TimeLineActivity.class);
                startActivity(intent);
            }
        });

    }

    public void startTimelineActivity(){
        Intent intent = new Intent(MainActivity.this, TimeLineActivity.class);
        startActivity(intent);
    }

}
