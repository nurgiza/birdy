package nurgiza.birdy;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.models.Tweet;
import com.twitter.sdk.android.core.services.StatusesService;

public class StatusActivity extends Activity implements View.OnClickListener, TextWatcher {
    private static final String TAG = StatusActivity.class.getSimpleName();
    private EditText editText;
    private Button updateButton;
    private TextView textCount;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status);

        initButton();
        initEdit();
        initTextCount();
    }

    private void initButton(){
        updateButton = (Button) findViewById(R.id.buttonUpdate);
        updateButton.setOnClickListener(this);
    }

    private void initEdit(){
        editText = (EditText) findViewById(R.id.editText);
        editText.addTextChangedListener(this);
    }

    private void initTextCount(){
        textCount = (TextView) findViewById(R.id.textCount);
        textCount.setText(Integer.toString(140));
        textCount.setTextColor(Color.GREEN);
    }

    public void onClick(View v) {
        Log.d(TAG, "onClick");

        String status = editText.getText().toString();
        if (status.length() > 0) {
            new PostTwitt().execute(status);
            startService(new Intent(this, UpdaterService.class));
        }

        //close activity
        finish();
    }

    class PostTwitt extends AsyncTask<String, Integer, String> {
        @Override
        protected String doInBackground(String... statuses) {
            try {
                final StatusesService statusesService = Twitter.getInstance().getApiClient().getStatusesService();
                statusesService.update(statuses[0], null, null, null, null, null, null,
                        null, null, new Callback<Tweet>() {
                            @Override
                            public void success(Result<Tweet> tweetResult) {
                                Toast.makeText(StatusActivity.this, getString(R.string.postOk),
                                        Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void failure(TwitterException e) {
                                Toast.makeText(StatusActivity.this, getString(R.string.postFail),
                                        Toast.LENGTH_SHORT).show();
                            }
                        });
                return "OK";
            } catch (TwitterException e) {
                Log.e(TAG, e.toString());
                e.printStackTrace();
                return "FAIL";
            }
        } //doInBackground

        @Override
        protected void onProgressUpdate(Integer... values) {
            Log.d(TAG, "onProgressUpdate");
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(String result) {
            Log.d(TAG, "onPostExecute");
            if (result.equals("OK"))
            {
                editText.setText("");
            }
        }
    } //PostTwitt

    public void afterTextChanged(Editable statusText) {
        Log.d(TAG, "afterTextChanged");

        int count = 140 - statusText.length();

        textCount.setText(Integer.toString(count));
        textCount.setTextColor(Color.GREEN);
        if (count < 10) {
            textCount.setTextColor(Color.YELLOW);
        }
        if (count < 0) {
            textCount.setTextColor(Color.RED);
        }
    }

    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }
}