package nurgiza.birdy;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class TweetDatabase {
    static final String TAG = TweetDatabase.class.getSimpleName();
    static final String DB_NAME = "timeline.db";
    static final int    DB_VERSION = 3;
    static final String TABLE = "timeline";

    static final String C_CREATED = "created";
    static final String C_TWEET_ID = "_id";
    static final String C_TEXT = "txt";
    static final String C_USER = "user";

    public static final String ORDER_BY_CREATION_DESC = C_CREATED + " DESC";

    public final DbHelper dbHelper;

    public TweetDatabase(Context context) {
        this.dbHelper = new DbHelper(context);
        Log.d(TAG, "Constructed");
    }

    public void close() {
        this.dbHelper.close();
        Log.d(TAG, "Close");
    }

    public long insertOrIgnore(ContentValues values) {
        Log.d(TAG, "insertOrIgnore on " + values);
        SQLiteDatabase db = this.dbHelper.getWritableDatabase();
        long insertedId = -1;
        try {
           insertedId = db.insertWithOnConflict(TABLE, null, values, SQLiteDatabase.CONFLICT_IGNORE);
            if (insertedId != -1)
                dbHelper.context.getContentResolver().notifyChange(DatabaseProvider.CONTENT_URI, null);
        } catch (Exception e){
            Log.d(TAG, "Close" + e.getMessage());
        }

        return insertedId;
    }

    public void delete() {
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        db.delete(TABLE, null, null);
        db.close();
    }

    public String getLastCreated(){
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        String created = "";

        Cursor cursor = db.query(TABLE, new String[] {C_CREATED}, null, null, null, null, null,  "1");
        if(cursor != null)
        {
            if (cursor.moveToFirst()) {
                created = cursor.getString( cursor.getColumnIndex(C_CREATED) );
            }
            cursor.close();
        }

        return created;
    }

    public String getCreatedById(long id){
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        String created = "";

        Cursor cursor = db.query(TABLE, new String[] {C_CREATED},
                C_TWEET_ID + " = ?",
                new String[] {Long.toString(id)},
                null, null, null,  "1");
        if(cursor != null)
        {
            if (cursor.moveToFirst()) {
                created = cursor.getString( cursor.getColumnIndex(C_CREATED) );
            }
            cursor.close();
        }

        return created;
    }

    public long countRows(String oldCreated, long lastId){
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        String newCteated = getCreatedById(lastId);

        long count =
                DatabaseUtils.queryNumEntries(db,
                TABLE, C_CREATED + " > ? AND " + C_CREATED + " <= ?",
                new String[] {oldCreated, newCteated});

        return count;
    }

    public class DbHelper extends SQLiteOpenHelper {

        Context context;

        public DbHelper(Context context) {
            super(context, DB_NAME, null, DB_VERSION);
            this.context = context;
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            String sql = context.getString(R.string.sqlCreate);

            Log.d(TAG, "onCreate: " + sql);

            db.execSQL(sql);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.d(TAG, "onUpgrade");

            db.execSQL("drop table if exists " + TABLE);

            this.onCreate(db);
        }

    }
}